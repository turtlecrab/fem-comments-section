# Frontend Mentor - Interactive comments section

![Preview for the Interactive comments section coding challenge](./design/result-screenshot.gif)

## Links

- Live site: <https://fem-comments-section.vercel.app/>
- Solution page: <https://www.frontendmentor.io/solutions/comment-section-plus-ChvL4F_Wif>

## The challenge

I changed the [expected behaviour](https://github.com/frontendmentorio/interactive-comments-section/blob/main/README.md#expected-behaviour) to make the challenge more interesting:

- You can reply to a comment at any depth (so it's a comment tree like on facebook)
- You can change the current user (via the button in the top-right)
- No sorting of first-level comments (it's easily done but boring and kinda useless here)

Also:

- `Ctrl`+`Enter` in the textarea submits the form
- You cannot add empty comments
- You cannot vote on your own comments
- A user can vote only `+1` or `-1` (no multiple votes)
- You can cancel your vote by voting the opposite
- Your vote is shown
- Cancel button is shown when updating the comment

## Notes

Very interesting project, took a lot of time though. At first I implemented the majority of functionality using _props-drilling_ 😈. Then moved the state to a `context` 🤷‍♂️. Then rewrote everything with `redux` 💪

## Stuff I used

- `timeago.js` to format the time
- `react-remove-scroll-bar` and `TrapFocus` from `@mui/base` for the modal
- `Drawer` from `@mui/material` for the users drawer

## Stuff I've learned

- How to use `redux-thunk`'s synchronous action-functions to extract non-pure logic out of components
- How to use `React.memo` with [redux docs best practices](https://redux.js.org/style-guide/#connect-more-components-to-read-data-from-the-store) to minimize rerenders
- How to make `gifs` with https://www.screentogif.com/ 😺

## Stuff I considered doing, maybe will do later

- make replies margins smaller when depth > 2
- make Time self-updating
- make duplicate toasts not stack
- remake comment layout with grid
- rewire mui to styled-components
  - https://github.com/mui/material-ui/tree/master/examples/create-react-app-with-styled-components
- implement adding new users
