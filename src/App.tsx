import styled from 'styled-components'

import CommentList from './components/CommentList'
import Attribution from './components/Attribution'
import NewCommentForm from './components/NewCommentForm'
import UsersDrawer from './components/UsersDrawer'
import { createComment } from './store/commentSlice'
import { useAppDispatch } from './store/store'

function App() {
  const dispatch = useAppDispatch()

  return (
    <>
      <Main>
        <UsersDrawer />
        <HeaderSR>Interactive comments section</HeaderSR>
        <CommentList threadId="root" />
        <NewCommentForm
          root={true}
          addComment={text => dispatch(createComment(text, 'root'))}
        />
      </Main>
      <footer>
        <Attribution />
      </footer>
    </>
  )
}

const HeaderSR = styled.h1`
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0, 0, 0, 0);
  white-space: nowrap;
  border-width: 0;
`
const Main = styled.main`
  width: 100%;
  padding: 32px 16px;

  @media ${props => props.theme.LG} {
    padding: 64px 16px 29px;
  }
`

export default App
