import styled from 'styled-components'

function Attribution() {
  return (
    <Container>
      Challenge by{' '}
      <a
        href="https://www.frontendmentor.io?ref=challenge"
        target="_blank"
        rel="noreferrer"
      >
        Frontend Mentor
      </a>
      . Coded by{' '}
      <a
        href="https://www.frontendmentor.io/profile/turtlecrab"
        target="_blank"
        rel="noreferrer"
      >
        Turte Crab
      </a>
      .
    </Container>
  )
}

const Container = styled.div`
  font-size: 12px;
  font-family: var(--ff);
  text-align: center;
  padding: 10px;
  min-height: 35px;
  color: var(--text-dark-color);

  a {
    color: var(--primary-blue);
  }
`

export default Attribution
