import styled from 'styled-components'

import iconReply from '../images/icon-reply.svg'
import iconDelete from '../images/icon-delete.svg'
import iconEdit from '../images/icon-edit.svg'
import iconCancel from '../images/icon-cancel.svg'

interface Props {
  editable: boolean
  isEditing: boolean
  onReply: () => void
  onDelete: () => void
  onEdit: () => void
  onCancel: () => void
}

function Buttons({
  editable,
  isEditing,
  onReply,
  onDelete,
  onEdit,
  onCancel,
}: Props) {
  return (
    <Container>
      {editable ? (
        isEditing ? (
          <Button
            $color="var(--primary-red)"
            $icon={iconCancel}
            onClick={onCancel}
          >
            Cancel
          </Button>
        ) : (
          <>
            <Button
              $color="var(--primary-red)"
              $icon={iconDelete}
              onClick={onDelete}
            >
              Delete
            </Button>
            <Button
              $color="var(--primary-blue)"
              $icon={iconEdit}
              onClick={onEdit}
            >
              Edit
            </Button>
          </>
        )
      ) : (
        <Button
          $color="var(--primary-blue)"
          $icon={iconReply}
          onClick={onReply}
        >
          Reply
        </Button>
      )}
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-end;

  @media ${props => props.theme.LG} {
    position: absolute;
    top: 28px;
    right: 24px;
  }
`

const Button = styled.button<{ $icon: string; $color: string }>`
  cursor: pointer;
  margin: 0 0 0 16px;
  padding: 4px 0 4px 22px;
  border: none;
  background-color: transparent;
  font-family: var(--ff);
  font-size: 16px;
  font-weight: 500;
  color: ${props => props.$color};
  background-image: url(${props => props.$icon});
  background-repeat: no-repeat;
  background-position: left center;

  &:hover {
    opacity: 0.5;
  }

  @media ${props => props.theme.LG} {
    margin-left: 24px;
  }
`

export default Buttons
