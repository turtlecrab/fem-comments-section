import styled from 'styled-components'
import { memo, useEffect, useRef, useState } from 'react'
import toast from 'react-hot-toast'

import Rating from './Rating'
import Buttons from './Buttons'
import CommentList from './CommentList'
import NewCommentForm from './NewCommentForm'
import DeleteModal from './DeleteModal'
import TimeAgo from './TimeAgo'
import { PrimaryButton } from './PrimaryButton'
import { Textarea } from './Textarea'
import { useAppDispatch, useAppSelector } from '../store/store'
import {
  createComment,
  deleteComment,
  editComment,
} from '../store/commentSlice'

interface Props {
  id: string
}

function Comment({ id }: Props) {
  const [isReplying, setIsReplying] = useState(false)
  const [isDeleting, setIsDeleting] = useState(false)
  const [isEditing, setIsEditing] = useState(false)

  const { content, createdAt, replyingTo, username, replies } = useAppSelector(
    state => state.comment.comments[id]
  )

  const byCurrentUser =
    useAppSelector(state => state.comment.currentUser) === username
  const avatar = useAppSelector(state => state.comment.users[username].avatar)

  const dispatch = useAppDispatch()

  const editTextareaRef = useRef<HTMLTextAreaElement>(null)
  useEffect(() => {
    if (editTextareaRef.current) {
      const end = editTextareaRef.current.value.length
      editTextareaRef.current.setSelectionRange(end, end)
    }
  }, [isEditing])

  function submitReplyAndHideForm(text: string) {
    dispatch(createComment(text, id))
    setIsReplying(false)
  }

  function submitUpdatedComment(newContent: string) {
    newContent = newContent.trim()
    if (!newContent) {
      toast.error('Comment cannot be empty')
      return
    }
    dispatch(editComment({ id, content: newContent }))
    setIsEditing(false)
  }

  function handleUpdate(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault()
    submitUpdatedComment(e.currentTarget.content.value)
  }

  function handleCtrlEnter(e: React.KeyboardEvent<HTMLTextAreaElement>) {
    if (e.ctrlKey && e.key === 'Enter') {
      e.currentTarget.blur()
      submitUpdatedComment(e.currentTarget.value)
    }
  }

  return (
    <>
      <CommentContainer>
        <Header>
          <Avatar src={avatar} alt="" />
          <Username>{username}</Username>
          {byCurrentUser && <YouBadge>You</YouBadge>}
          <TimeAgo time={createdAt} />
        </Header>
        {isEditing ? (
          <EditCommentForm onSubmit={handleUpdate}>
            <Textarea
              autoFocus
              id="content"
              defaultValue={content}
              ref={editTextareaRef}
              onKeyDown={handleCtrlEnter}
            />
            <PrimaryButton type="submit">Update</PrimaryButton>
          </EditCommentForm>
        ) : (
          <Text>
            {replyingTo && <ReplyTo>@{replyingTo} </ReplyTo>}
            {content}
          </Text>
        )}
        <BottomWrapper>
          <Rating id={id} />
          <Buttons
            editable={byCurrentUser}
            isEditing={isEditing}
            onReply={() => setIsReplying(prev => !prev)}
            onDelete={() => setIsDeleting(true)}
            onEdit={() => setIsEditing(true)}
            onCancel={() => setIsEditing(false)}
          />
        </BottomWrapper>
        {isDeleting && (
          <DeleteModal
            onCancel={() => setIsDeleting(false)}
            onDelete={() => dispatch(deleteComment(id))}
          />
        )}
      </CommentContainer>
      {isReplying && <NewCommentForm addComment={submitReplyAndHideForm} />}
      {replies && replies.length > 0 && (
        <RepliesContainer>
          <CommentList threadId={id} />
        </RepliesContainer>
      )}
    </>
  )
}

const CommentContainer = styled.section`
  position: relative;
  background-color: var(--white);
  border-radius: 8px;
  padding: 16px;

  @media ${props => props.theme.LG} {
    padding: 24px 24px 24px 88px;
    min-height: 148px;
  }
`

const Header = styled.header`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  gap: 16px;

  @media ${props => props.theme.LG} {
    padding-right: 164px;
  }
`

const Avatar = styled.img`
  width: 32px;
  height: 32px;
`

const Username = styled.h2`
  margin: 0;
  padding: 0;
  font-family: var(--ff);
  font-size: 16px;
  font-weight: 500;
  color: var(--text-dark-color);
`

const YouBadge = styled.div`
  padding: 2px 7px;
  margin-left: -8px;
  border-radius: 2px;
  background-color: var(--primary-blue);
  color: var(--white);
  font-family: var(--ff);
  font-size: 12px;
  font-weight: 500;
  text-transform: lowercase;
`

const EditCommentForm = styled.form`
  margin: 16px 0;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  gap: 16px;

  @media ${props => props.theme.LG} {
    margin-bottom: 0;
  }
`

const Text = styled.p`
  margin: 16px 0;
  padding: 0;
  font-family: var(--ff);
  font-size: 16px;
  font-weight: 400;
  line-height: 150%;
  color: var(--text-light-color);
  white-space: pre-line;

  @media ${props => props.theme.LG} {
    margin-bottom: 0;
  }
`

const ReplyTo = styled.span`
  font-weight: 500;
  color: var(--primary-blue);
`

const BottomWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const RepliesContainer = styled.div`
  border-left: 2px solid var(--vertical-line-color);
  padding-left: 18px;

  @media ${props => props.theme.LG} {
    padding-left: 43px;
    margin-left: 43px;
  }
`

export default memo(Comment)
