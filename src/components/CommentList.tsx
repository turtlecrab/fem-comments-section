import styled from 'styled-components'
import { memo } from 'react'

import Comment from './Comment'
import { useAppSelector } from '../store/store'

interface Props {
  threadId: string
}

function CommentList({ threadId }: Props) {
  const commentsToShow = useAppSelector(state => {
    return threadId === 'root'
      ? state.comment.rootReplies
      : state.comment.comments[threadId].replies
  })

  return (
    <Container>
      {commentsToShow.map(commentId => (
        <Comment id={commentId} key={commentId} />
      ))}
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  gap: 16px;
  max-width: ${props => props.theme.maxWidth};
  margin: auto;

  @media ${props => props.theme.LG} {
    gap: 20px;
  }
`

export default memo(CommentList)
