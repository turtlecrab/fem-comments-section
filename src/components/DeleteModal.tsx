import { RemoveScrollBar } from 'react-remove-scroll-bar'
import styled from 'styled-components'
import TrapFocus from '@mui/base/TrapFocus'

interface Props {
  onCancel: () => void
  onDelete: () => void
}

function DeleteModal({ onCancel, onDelete }: Props) {
  return (
    <Overlay onClick={onCancel}>
      <TrapFocus open>
        <Container tabIndex={-1} onClick={e => e.stopPropagation()}>
          <Header>Delete comment</Header>
          <Text>
            Are you sure you want to delete this comment? This will remove the
            comment and can’t be undone.
          </Text>
          <ButtonWrapper>
            <Button $color="var(--text-light-color)" onClick={onCancel}>
              No, cancel
            </Button>
            <Button $color="var(--primary-red)" onClick={onDelete}>
              Yes, delete
            </Button>
          </ButtonWrapper>
        </Container>
      </TrapFocus>
      <RemoveScrollBar />
    </Overlay>
  )
}

const Overlay = styled.div`
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100vw;
  height: 100vh;
  z-index: 10;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #0008;
`

const Container = styled.section`
  padding: 24px 28px;
  margin: 16px;
  background-color: var(--white);
  border-radius: 8px;
  max-width: 400px;

  @media ${props => props.theme.LG} {
    padding: 32px;
  }
`

const Header = styled.header`
  color: var(--text-dark-color);
  font-family: var(--ff);
  font-size: 20px;
  font-weight: 500;

  @media ${props => props.theme.LG} {
    font-size: 24px;
  }
`

const Text = styled.p`
  color: var(--text-light-color);
  font-family: var(--ff);
  font-size: 16px;
  font-weight: 400;
  line-height: 150%;
  margin: 16px 0;

  @media ${props => props.theme.LG} {
    margin: 20px 0;
  }
`

const ButtonWrapper = styled.footer`
  display: flex;
  gap: 12px;
`

const Button = styled.button<{ $color: string }>`
  flex: 1;
  cursor: pointer;
  border: none;
  border-radius: 8px;
  padding: 15px 10px;
  background-color: ${props => props.$color};
  color: var(--white);
  font-family: var(--ff);
  font-size: 16px;
  font-weight: 500;
  text-transform: uppercase;
`

export default DeleteModal
