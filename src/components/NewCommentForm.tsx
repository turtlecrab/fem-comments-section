import { useEffect, useRef, useState } from 'react'
import styled from 'styled-components'
import toast from 'react-hot-toast'

import { PrimaryButton } from './PrimaryButton'
import { Textarea } from './Textarea'
import { useAppSelector } from '../store/store'

interface Props {
  addComment: (text: string) => void
  root?: boolean
}

function NewCommentForm({ addComment, root }: Props) {
  const [text, setText] = useState('')
  const inputRef = useRef<HTMLTextAreaElement>(null)

  const avatar = useAppSelector(
    state => state.comment.users[state.comment.currentUser].avatar
  )

  useEffect(() => {
    if (!root) inputRef.current?.focus()
  }, [root])

  function submitComment(text: string) {
    text = text.trim()
    if (!text) {
      toast.error('Comment cannot be empty')
      return
    }
    addComment(text)
    setText('')
  }

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault()
    submitComment(e.currentTarget.text.value)
  }

  function handleCtrlEnter(e: React.KeyboardEvent<HTMLTextAreaElement>) {
    if (e.ctrlKey && e.key === 'Enter') {
      e.currentTarget.blur()
      submitComment(e.currentTarget.value)
    }
  }

  return (
    <Form onSubmit={handleSubmit} $root={Boolean(root)}>
      <Textarea
        cols={30}
        rows={3}
        placeholder="Add a comment..."
        name=""
        id="text"
        value={text}
        onChange={e => setText(e.currentTarget.value)}
        onKeyDown={handleCtrlEnter}
        ref={inputRef}
      ></Textarea>
      <Row>
        <Avatar src={avatar} alt="" />
        <PrimaryButton type="submit">{root ? 'Send' : 'Reply'}</PrimaryButton>
      </Row>
    </Form>
  )
}

const Form = styled.form<{ $root: boolean }>`
  margin-top: ${props => (props.$root ? '16px' : '-8px')};
  padding: 16px 13px 13px;
  background-color: var(--white);
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: ${props => props.theme.maxWidth};
  margin-inline: auto;
  position: relative;

  @media ${props => props.theme.LG} {
    margin-top: ${props => (props.$root ? '20px' : '-8px')};
    padding: 24px 24px 24px 80px;
    flex-direction: row;
  }
`

const Row = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 16px;

  @media ${props => props.theme.LG} {
    align-items: start;
    margin: 0 0 0 16px;
  }
`

const Avatar = styled.img`
  width: 32px;
  height: 32px;

  @media ${props => props.theme.LG} {
    position: absolute;
    top: 28px;
    left: 24px;
    width: 40px;
    height: 40px;
  }
`

export default NewCommentForm
