import styled from 'styled-components'

export const PrimaryButton = styled.button`
  cursor: pointer;
  border: none;
  border-radius: 8px;
  padding: 15px;
  min-width: 104px;
  background-color: var(--primary-blue);
  color: var(--white);
  font-family: var(--ff);
  font-size: 16px;
  font-weight: 500;
  text-transform: uppercase;

  &:hover {
    opacity: 0.4;
  }
`
