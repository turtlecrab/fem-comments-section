import styled from 'styled-components'
import toast from 'react-hot-toast'
import { memo } from 'react'

import { useAppSelector } from '../store/store'

import plusIcon from '../images/icon-plus.svg'
import minusIcon from '../images/icon-minus.svg'
import { useDispatch } from 'react-redux'
import { voteOnComment } from '../store/commentSlice'

interface Props {
  id: string
}

function Rating({ id }: Props) {
  const comment = useAppSelector(state => state.comment.comments[id])
  const currentUser = useAppSelector(state => state.comment.currentUser)
  const curVote = comment.score[currentUser]

  const dispatch = useDispatch()

  function submitVote(id: string, vote: string) {
    if (curVote === vote) return
    if (comment.username === currentUser) {
      toast.error('You cannot vote on your own comments')
      return
    }
    dispatch(voteOnComment({ id, vote }))
  }

  const isOwnComment = comment.username === currentUser
  const score =
    Object.values(comment.score).reduce((acc, cur) => {
      if (cur === '+') return acc + 1
      if (cur === '-') return acc - 1
      return acc
    }, 0) + comment.baseScore // for screenshot accuracy

  return (
    <Container>
      <Button
        aria-label="Upvote the comment"
        className={
          (curVote === '+' ? 'selected' : '') + (isOwnComment ? ' own' : '')
        }
        onClick={() => submitVote(id, '+')}
      >
        <img src={plusIcon} alt="" />
      </Button>
      <RatingValue>{score}</RatingValue>
      <Button
        aria-label="Downvote the comment"
        className={
          (curVote === '-' ? 'selected' : '') + (isOwnComment ? ' own' : '')
        }
        onClick={() => submitVote(id, '-')}
      >
        <img src={minusIcon} alt="" />
      </Button>
    </Container>
  )
}

const Container = styled.div`
  background-color: var(--rating-bg-color);
  min-width: 100px;
  min-height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 13px;
  border-radius: 10px;

  @media ${props => props.theme.LG} {
    flex-direction: column;
    min-width: 40px;
    min-height: 100px;
    padding: 9px 0;
    position: absolute;
    left: 24px;
    top: 24px;
  }
`

const Button = styled.button`
  cursor: pointer;
  padding: 0;
  margin: 0;
  border: none;
  background-color: transparent;
  width: 16px;
  height: 16px;
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover,
  &.selected {
    filter: brightness(0) saturate(100%) invert(35%) sepia(20%) saturate(1878%)
      hue-rotate(200deg) brightness(95%) contrast(92%);
  }

  &.selected {
    cursor: default;
  }

  &.own {
    cursor: default;
    filter: none;
  }
`

const RatingValue = styled.div`
  font-family: var(--ff);
  font-size: 17px;
  font-weight: 500;
  color: var(--primary-blue);
`

export default memo(Rating)
