import styled from 'styled-components'

export const Textarea = styled.textarea`
  border: 1px solid var(--vertical-line-color);
  border-radius: 8px;
  width: 100%;
  min-height: 96px;
  padding: 14px 24px;
  color: var(--text-dark-color);
  font-family: var(--ff);
  font-size: 16px;
  font-weight: 400;

  &::placeholder {
    color: var(--text-light-color);
  }
`
