import styled from 'styled-components'
import { format } from 'timeago.js'

interface Props {
  time: string
}

// TODO: make it self update
function TimeAgo({ time }: Props) {
  if (/^\d+$/.test(time)) {
    time = format(time)
  }
  return <Time>{time}</Time>
}

const Time = styled.time`
  font-family: var(--ff);
  font-size: 16px;
  font-weight: 400;
  color: var(--text-light-color);
`
export default TimeAgo
