import { useState } from 'react'
import { useDispatch } from 'react-redux'
import styled from 'styled-components'
import Drawer from '@mui/material/Drawer'
import { zeroRightClassName } from 'react-remove-scroll-bar'

import { useAppSelector } from '../store/store'
import { switchUser } from '../store/commentSlice'

function UsersDrawer() {
  const [isOpen, setIsOpen] = useState(false)

  const dispatch = useDispatch()
  const currentUser = useAppSelector(state => state.comment.currentUser)
  const users = useAppSelector(state => state.comment.users)

  function selectUser(username: string) {
    setIsOpen(false)
    dispatch(switchUser(username))
  }

  return (
    <>
      <ButtonContainer className={zeroRightClassName}>
        <DrawerButton
          aria-label="Open user selection menu"
          onClick={() => setIsOpen(true)}
        >
          ∷
        </DrawerButton>
      </ButtonContainer>
      <Drawer anchor="right" open={isOpen} onClose={() => setIsOpen(false)}>
        <DrawerContainer>
          <DrawerButton
            onClick={() => setIsOpen(false)}
            style={{ alignSelf: 'end' }}
          >
            &times;
          </DrawerButton>
          <Header>Choose a user:</Header>
          <List>
            {Object.entries(users).map(([username, { avatar }]) => (
              <li key={username}>
                <UserButton
                  className={currentUser === username ? 'current' : ''}
                  onClick={() => selectUser(username)}
                >
                  <img src={avatar} alt="" /> {username}
                </UserButton>
              </li>
            ))}
          </List>
        </DrawerContainer>
      </Drawer>
    </>
  )
}

const ButtonContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  z-index: 5;

  @media ${props => props.theme.LG} {
    position: fixed;
  }
`

const DrawerButton = styled.button`
  width: 48px;
  height: 48px;
  background-color: var(--white);
  border: none;
  border-radius: 0 0 0 8px;
  box-shadow: 0 0 8px 8px rgba(0, 0, 0, 0.03);
  cursor: pointer;
  font-size: 24px;
`

const Header = styled.h2`
  margin: -10px 20px 20px;
  color: var(--text-dark-color);
  font-family: var(--ff);
  font-size: 20px;
  font-weight: 500;
`

const DrawerContainer = styled.div`
  width: 250px;
  display: flex;
  flex-direction: column;
`

const List = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`

const UserButton = styled.button`
  padding: 10px 20px;
  cursor: pointer;
  width: 100%;
  display: flex;
  align-items: center;
  gap: 16px;
  border: none;
  font-family: var(--ff);
  font-size: 16px;
  font-weight: 500;
  color: var(--text-dark-color);
  background-color: var(--body-bg-color);

  &.current {
    &::after {
      content: 'you';
      padding: 2px 7px;
      margin-left: -8px;
      border-radius: 2px;
      background-color: var(--primary-blue);
      color: var(--white);
      font-family: var(--ff);
      font-size: 12px;
      font-weight: 500;
    }
  }

  &:hover {
    background-color: var(--primary-red);
  }

  & > img {
    width: 64px;
    height: 64px;
  }
`

export default UsersDrawer
