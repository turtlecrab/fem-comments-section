export interface IComment {
  id: string
  content: string
  createdAt: string
  baseScore: number
  score: {
    [key: string]: string
  }
  replyingTo?: string
  username: string
  replies: string[]
}

export interface ICommentState {
  currentUser: string
  users: {
    [key: string]: {
      avatar: string
    }
  }
  rootReplies: string[]
  comments: {
    [key: string]: IComment
  }
}
