import { createSlice, nanoid, PayloadAction } from '@reduxjs/toolkit'

import { IComment, ICommentState } from '../interfaces'

import initialState from '../data.json'
import { AppDispatch } from './store'

const commentSlice = createSlice({
  name: 'comments',
  initialState: initialState as ICommentState,
  reducers: {
    addComment(
      state,
      action: PayloadAction<{
        id: string
        content: string
        time: string
        replyToId: string
      }>
    ) {
      const { id, content, time, replyToId } = action.payload
      const newComment: IComment = {
        id: id,
        content,
        createdAt: time,
        baseScore: 0,
        score: {},
        username: state.currentUser,
        replies: [],
      }
      if (replyToId === 'root') {
        state.rootReplies.push(id)
      } else {
        newComment.replyingTo = state.comments[replyToId].username
        state.comments[replyToId].replies.push(id)
      }
      state.comments[id] = newComment
    },
    deleteComment(state, action: PayloadAction<string>) {
      const idToDelete = action.payload

      function deleteRecur(id: string) {
        if (state.comments[id].replies.length > 0) {
          state.comments[id].replies.forEach(replyId => {
            deleteRecur(replyId)
          })
        }
        delete state.comments[id]
      }
      deleteRecur(idToDelete)

      // TODO: change state 'replyTo' property to be a comment id instead of a name?
      //       so here we just `state.comment[replyTo].replies.filter(id => id !== delId)
      Object.values(state.comments).forEach(comment => {
        if (comment.replies.includes(idToDelete)) {
          comment.replies = comment.replies.filter(
            replyId => replyId !== idToDelete
          )
        }
      })
      if (state.rootReplies.includes(idToDelete)) {
        state.rootReplies = state.rootReplies.filter(
          replyId => replyId !== idToDelete
        )
      }
    },
    editComment(state, action: PayloadAction<{ id: string; content: string }>) {
      const { id, content } = action.payload
      state.comments[id].content = content
    },
    voteOnComment(state, action: PayloadAction<{ id: string; vote: string }>) {
      const { id, vote } = action.payload
      const prevVote = state.comments[id].score[state.currentUser]
      if (
        (prevVote === '-' && vote === '+') ||
        (prevVote === '+' && vote === '-')
      ) {
        delete state.comments[id].score[state.currentUser]
      } else {
        state.comments[id].score[state.currentUser] = vote
      }
    },
    switchUser(state, action: PayloadAction<string>) {
      const username = action.payload
      if (Object.keys(state.users).includes(username)) {
        state.currentUser = username
      }
    },
  },
})

export function createComment(content: string, replyToId: string) {
  return (dispatch: AppDispatch) => {
    const id = nanoid()
    const time = String(Date.now())
    dispatch(
      addComment({
        id,
        content,
        time,
        replyToId,
      })
    )
  }
}

export default commentSlice.reducer
export const {
  addComment,
  deleteComment,
  editComment,
  voteOnComment,
  switchUser,
} = commentSlice.actions
